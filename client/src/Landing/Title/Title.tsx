import React from 'react'
import style from './style.module.scss'

export default ({children}: {children: React.ReactNode}) =>
  <div className={style.title}>{children}</div>
