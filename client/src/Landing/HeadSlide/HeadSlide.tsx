import React from 'react'
import style from './style.module.scss'
import apollo from 'assets/images/apollo.svg'
import express from 'assets/images/express.svg'
import gatsby from 'assets/images/gatsby.svg'
import javascript from 'assets/images/javascript.svg'
import jest from 'assets/images/jest.svg'
import material from 'assets/images/material.svg'
import mobx from 'assets/images/mobx.svg'
import mongodb from 'assets/images/mongodb.svg'
import nest from 'assets/images/nest.svg'
import next from 'assets/images/next.svg'
import nginx from 'assets/images/nginx.svg'
import node from 'assets/images/node.svg'
import postgres from 'assets/images/postgres.svg'
import redux from 'assets/images/redux.svg'
import rollup from 'assets/images/rollup.svg'
import sass from 'assets/images/sass.svg'
import stripe from 'assets/images/stripe.svg'
import tux from 'assets/images/tux.svg'
import typescript from 'assets/images/typescript.svg'
import webpack from 'assets/images/webpack.svg'

const logos = [
  apollo, express, gatsby, javascript, jest, material, mobx, mongodb, nest, next,
  nginx, node, postgres, redux, rollup, sass, stripe, tux, typescript, webpack,
].map(src => new Promise<HTMLImageElement>(resolve => {
  const image = new Image()
  image.onload = () => {
    (image as any).loaded = true
    resolve(image)
  }
  image.src = src
  return image
}))

const margin = 20

export default () => {
  const canvas = React.useRef(null)

  React.useEffect(() => {
    const el = (canvas.current as any)
    const width = el.width = el.parentNode.offsetWidth
    const height = el.height = el.parentNode.offsetHeight
    const ctx = el.getContext('2d')
    let animationFrame: any

    const items: {image: HTMLImageElement, y: number, x: number}[] = []

    const getRandomCoords: (image: HTMLImageElement, i?: number) => undefined | {x: number, y: number} =
      (image, i = 0) => {
        const y = ~~(height * Math.random())
        const x = ~~((width - image.width - 40) * Math.random()) + 20
        const right = x + image.width
        const bottom = y + image.height
        if (items.some(item =>
          !(
            item.x - margin > right ||
            item.x + item.image.width + margin < x ||
            item.y - margin > bottom ||
            item.y + item.image.height + margin < y
          )
        )) return i === 100 ? undefined : getRandomCoords(image, i + 1)
        return {x, y}
      }

    logos.forEach(async (promise) => {
      const image = await promise
      const coords = getRandomCoords(image)
      if (coords)
        items.push({image, ...coords})
    })

    const animation = () => {
      ctx.clearRect(0, 0, width, height)
      items.forEach(item => {
        if ((item.image as any).loaded)
          ctx.drawImage(item.image, item.x, item.y)
        item.y -= .5
        if (item.y + item.image.height < 0)
          item.y = height
      })
    }

    const interval = setInterval(animation, 50)

    return () => clearInterval(interval)
  }, [])

  return <div className={style.page}>
    <canvas ref={canvas} className={style.canvas}/>
    <div className={style.title}>
      <div className={style.name}>Roman Kushyn</div>
      <div className={style.jobTitle}>Node.js & React Developer</div>
    </div>
  </div>
}
