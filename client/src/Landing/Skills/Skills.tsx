import React from 'react'
import Slide from '../Slide/Slide'
import Title from '../Title/Title'
import style from './style.module.scss'

export default () =>
  <Slide>
    <Title>Skills</Title>
    <table className={style.table}>
      <tbody>
      <tr className={style.skill}>
        <td className={style.skillName}>Node.js</td>
        <td className={style.skillText}>express, nest.js, next.js, gatsby, postgraphile</td>
      </tr>
      <tr className={style.skill}>
        <td className={style.skillName}>React</td>
        <td className={style.skillText}>material-ui, MobX, apollo</td>
      </tr>
      <tr className={style.skill}>
        <td className={style.skillName}>Redux</td>
        <td className={style.skillText}>thunks, sagas, rematch</td>
      </tr>
      <tr className={style.skill}>
        <td className={style.skillName}>Linux</td>
        <td className={style.skillText}>Desktop Linux user, can configure servers, Nginx</td>
      </tr>
      <tr className={style.skill}>
        <td className={style.skillName}>SQL</td>
        <td className={style.skillText}>can write and optimize complex queries for search and statistics</td>
      </tr>
      <tr className={style.skill}>
        <td className={style.skillName}>Payment integrations</td>
        <td className={style.skillText}>Stripe, WePay, Yandex Money</td>
      </tr>
      <tr className={style.skill}>
        <td className={style.skillName}>HTML/CSS</td>
        <td className={style.skillText}>familiar with sass/scss and stylus, know bootstrap and MaterialUI React library</td>
      </tr>
      <tr className={style.skill}>
        <td className={style.skillName}>NoSQL</td>
        <td className={style.skillText}>MongoDB, Neo4j, Redis</td>
      </tr>
      </tbody>
    </table>
    <div className={style.smallTitle}>Additional Skills</div>
    <ul className={style.ul}>
      <li className={style.li}>
        <div className={style.liText}>was making outreach system which acts like email client, so familiar with SMPT protocol</div>
      </li>
      <li className={style.li}>
        <div className={style.liText}>have experience with OpenResty, it's nginx extension that allows to write logic on proxy-level, which can be more efficient than writing it on node.js or other language under that nginx-proxy.</div>
      </li>
    </ul>
  </Slide>
