import React from 'react'
import cn from 'classnames'
import style from './style.module.scss'

type Props = {
  current: number,
  open: (number: number) => any
}

const Item = ({current, open, name, number}: Props & {name: string, number: number}) =>
  <div
    className={cn(style.item, number < current && style.prev || current === number && style.active)}
    onClick={() => open(number)}
  >
    <span>{name}</span>
  </div>

export default ({current, open}: Props) => {
  return <div className={style.menu}>
    <Item current={current} open={open} name={'Skills'} number={1} />
    <Item current={current} open={open} name={'Contacts'} number={2} />
  </div>
}
