import React from 'react'
import Slide from '../Slide/Slide'
import Title from '../Title/Title'
import style from './style.module.scss'
import email from 'assets/images/icons/email.svg'
import telegram from 'assets/images/icons/telegram.svg'
import upwork from 'assets/images/icons/upwork.svg'

export default () =>
  <Slide>
    <Title>Contacts</Title>
    <div className={style.contacts}>
      <a href='mailto:romdazao@gmail.com' className={style.contact}>
        <img className={style.icon} src={email}/>
        <div className={style.contactText}>romadzao@gmail.com</div>
      </a>
      <a href='https://t.me/romeerez' target='_blank' className={style.contact}>
        <img className={style.icon} src={telegram}/>
        <div className={style.contactText}>t.me/romeerez</div>
      </a>
      <a href='https://www.upwork.com/o/profiles/users/~013690ed362b648265/' target='_blank' className={style.contact}>
        <img className={style.icon} src={upwork}/>
        <div className={style.contactText}>upwork</div>
      </a>
    </div>
  </Slide>
