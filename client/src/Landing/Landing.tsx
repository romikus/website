import React from 'react'
import HeadSlide from './HeadSlide/HeadSlide'
import Skills from './Skills/Skills'
import Swipe from './Swipe/Swipe'
import Contacts from './Contacts/Contacts'
import Menu from './Menu/Menu'
import cn from 'classnames'
import style from './style.module.scss'

const slides = [HeadSlide, Skills, Contacts]

type ContentProps = {
  prevSlide: () => any,
  nextSlide: () => any,
  current: number,
  prev: number
}

const Content = ({prevSlide, nextSlide, current, prev}: ContentProps) =>
  <Swipe prev={prevSlide} next={nextSlide}>
    {slides.map((Slide, i) => {
      if (i !== current && i !== prev)
        return null

      return <div key={i} className={
        cn(
          style.screenWrapper,
          i < current ? style.up : i > current ? style.down : style.current
        )
      }>
        <div className={style.blockScroller}>
          <Slide/>
        </div>
      </div>
    })}
  </Swipe>

export default () => {
  const [prev, setPrev] = React.useState(0)
  const [current, setCurrent] = React.useState(0)
  const prevSlide = () => {
    if (current === 0)
      return
    setPrev(current)
    setCurrent(current - 1)
  }
  const nextSlide = () => {
    if (current + 1 === slides.length)
      return
    setPrev(current)
    setCurrent(current + 1)
  }

  return <div className={style.landing}>
    <Menu current={current} open={setCurrent} />
    <Content prevSlide={prevSlide} nextSlide={nextSlide} current={current} prev={prev} />
  </div>
}
