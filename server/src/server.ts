import express from 'express'
import path from 'path'

const app = express()
app.use(express.static('../client/build'))
const frontendFile = path.resolve(__dirname, '..', '..', 'client/build/index.html')
app.get('*', (req, res) => {
  res.sendFile(frontendFile)
})

app.listen(process.env.PORT || 3000)
